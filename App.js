import {StatusBar} from 'expo-status-bar';
import React, {useState, useEffect} from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    Dimensions,
    Button,
    ScrollView,
    FlatList,
    SectionList,
    ActivityIndicator,
    Image,
    ImageBackground,
    Modal,
    Alert
} from 'react-native';
import * as Location from 'expo-location';
import Constants from 'expo-constants';
import MapView, {Marker} from 'react-native-maps';
import { Camera } from 'expo-camera';

const {width, height} = Dimensions.get('window')

const styles = StyleSheet.create({
    container: {
        flex: 1, // 100%
        flexDirection: 'column', // row | column
        backgroundColor: '#fff',
        alignItems: 'center', // flex-start | flex-end | center | stretch | baseline
        justifyContent: 'center', // flex-start | flex-end | center | space-between | space-around | space-evenly
    },
    red: {
        // flex: 1, // 1/6 del alto
        backgroundColor: 'red'
    },
    green: {
        // flex: 2, // 2/6 del alto
        backgroundColor: 'green'
    },
    black: {
        // flex: 3, // 3/6 del alto
        backgroundColor: 'black'
    },
    yellow: {
        backgroundColor: 'yellow'
    },
    cyan: {
        backgroundColor: 'cyan'
    },
    text: {
        fontSize: 30,
        color: 'white',
        width: '50%',
        height: 100
    },
    textInput: {
        height: 40,
        width: width,
        padding: 10,
        backgroundColor: '#eee',
        borderBottomWidth: 1,
        borderBottomColor: '#eab'
    },
    scrollView: {
        width: width,
        height: height
    },
    loading: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    listItem: {
        height: 50,
        fontSize: 20,
        borderBottomWidth: 1,
        borderBottomColor: '#eee'
    },
    photo: {
        height: 200,
        width: 200
    },
    center: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    map: {
        width,
        height,
        flex: 1,
    },
    camera: {
        flex: 1,
        width,
        height
    }
});

const TextComponent = ({text, style, children}) => {
    const [texto, setTexto] = useState(text || 'Hello 👋🏻')
    const onPress = () => setTexto("Adios 😔")
    return (
        <>
            <Text style={[styles.text, style]} onPress={onPress}>{texto}</Text>
            {children ?? children}
        </>
    )
}

const Loading = () => <View style={styles.loading}><Text>Cargando...</Text></View>

export const Scroll = ({children}) => <ScrollView style={styles.scrollView}>{children}</ScrollView>

const items = [
    {
        key: '1',
        name: 'texto 1',
        color: {...styles.red, height: 100}
    },
    {
        key: '2',
        name: 'texto 2',
        color: {...styles.green, height: 100}
    },
    {
        key: '3',
        name: 'texto 3',
        color: {...styles.black, height: 100}
    },
    {
        key: '4',
        name: 'texto 4',
        color: {...styles.yellow, height: 100}
    },
    {
        key: '5',
        name: 'texto 5',
        color: {...styles.cyan, height: 100}
    },
    {
        key: '6',
        name: 'texto 6',
        color: {...styles.red, height: 100}
    },
    {
        key: '7',
        name: 'texto 7',
        color: {...styles.green, height: 100}
    },
    {
        key: '8',
        name: 'texto 8',
        color: {...styles.black, height: 100}
    },
    {
        key: '9',
        name: 'texto 9',
        color: {...styles.yellow, height: 100}
    },
    {
        key: '10',
        name: 'texto 10',
        color: {...styles.cyan, height: 100}
    },
    {
        key: '11',
        name: 'texto 11',
        color: {...styles.red, height: 100}
    },
    {
        key: '12',
        name: 'texto 12',
        color: {...styles.green, height: 100}
    },
    {
        key: '13',
        name: 'texto 13',
        color: {...styles.black, height: 100}
    },
    {
        key: '14',
        name: 'texto 14',
        color: {...styles.yellow, height: 100}
    },
    {
        key: '15',
        name: 'texto 15',
        color: {...styles.cyan, height: 100}
    },
]

const section = [
    {
        title: 'Grupo 1',
        data: items.filter(item => Number(item.key) <= 5),
        style: {fontSize: 20, fontWeight: 'bold', backgroundColor: '#777777'}
    },
    {
        title: 'Grupo 2',
        data: items.filter(item => Number(item.key) > 5 && Number(item.key) <= 10),
        style: {fontSize: 20, fontWeight: 'bold', backgroundColor: '#777777'}
    },
    {
        title: 'Grupo 3',
        data: items.filter(item => Number(item.key) > 10 && Number(item.key) <= 15),
        style: {fontSize: 20, fontWeight: 'bold', backgroundColor: '#777777'}
    },
]

const createDialog = () =>
    Alert.alert(
        'Titulo alert',
        'Subtitle',
        [
            {
                text: 'cancelar',
                onPress: () => console.log('press cancel button')
            },
            {
                text: 'aceptar',
                onPress: () => console.log('press accept button')
            }
        ],
        {cancelable: false}
    )

export default function App() {
    const [users, setUsers] = useState([])
    const [loading, setLoading] = useState(true)
    const [modal, setModal] = useState(false)
    const [locationState, setLocationState] = useState({})
    const [access, setAccess] = useState(null)
    const [cameraType, setCametaType] = useState(Camera.Constants.Type.back)

    useEffect(() => {
            fetch('http://jsonplaceholder.typicode.com/users')
                .then(response => response.json())
                .then(data => {
                    setUsers(data)
                    setLoading(false)
                })
        }, []
    )

    const searchLocation = async () => {
        const {status} = await Location.requestPermissionsAsync()
        if(status !== 'granted') {
            return Alert.alert('Sin permisos')
        }
        const location = await Location.getCurrentPositionAsync({})
        // console.log('location: ', location);
        setLocationState(location)
    }

    const getAccess = async () => {
        const {status} = await Camera.requestPermissionsAsync()
        setAccess(status == 'granted')
        console.log('***************** status: ', status);
    }

    useEffect(() => {
        // searchLocation()
        getAccess()
    }, [])

    if (loading)
        return <Loading/>

    if (access === null) {
        return (
            <View>
                <Text>Esperando Permisos</Text>
            </View>
        )
    }

    if (access === false) {
        return (
            <View>
                <Text>No tenemos acceso a la camara</Text>
            </View>
        )
    }

    const changeCameraType = () => {
        const {back, front} = Camera.Constants.Type;
        const type = cameraType === back ? front : back
        setCametaType(type)
    }

    return (
        <View style={styles.container}>
            {/*<Scroll>*/}
            {/*    <StatusBar style="auto"/>*/}
            {/*    <TextComponent text="Hola Mundo aquí estoy" style={styles.red}>*/}
            {/*        <View>*/}
            {/*            <Text>Children</Text>*/}
            {/*        </View>*/}
            {/*    </TextComponent>*/}
            {/*    <TextComponent text="Chao Mundo" style={styles.green}/>*/}
            {/*    <TextComponent style={styles.black}/>*/}
            {/*    <TextComponent style={styles.black}/>*/}
            {/*    <TextComponent style={styles.black}/>*/}
            {/*    <TextComponent style={styles.black}/>*/}
            {/*    <TextComponent style={styles.black}/>*/}
            {/*    <TextComponent style={styles.black}/>*/}
            {/*    <TextComponent style={styles.black}/>*/}
            {/*    <TextComponent style={styles.black}/>*/}
            {/*    <TextComponent style={styles.black}/>*/}
            {/*    <TextComponent style={styles.black}/>*/}
            {/*    <TextComponent style={styles.black}/>*/}
            {/*    <TextComponent style={styles.black}/>*/}
            {/*    <TextInput style={styles.textInput} placeholder="Escribe" onChangeText={console.log} />*/}
            {/*    <Button title="Alert" onPress={() => alert('this is a Alert')} />*/}
            {/*</Scroll>*/}

            {/*<FlatList data={items} renderItem={({item}) => <Text style={item.color} >{item.name}</Text>} style={{width: '100%'}}>*/}
            {/*</FlatList>*/}

            {/*<SectionList*/}
            {/*    sections={section}*/}
            {/*    renderItem={({item}) => <Text style={item.color}>{item.name}</Text>}*/}
            {/*    renderSectionHeader={({section}) => <Text style={section.style}>{section.title}</Text>}>*/}
            {/*</SectionList>*/}

            {/*<FlatList*/}
            {/*    data={users}*/}
            {/*    renderItem={({item}) => <Text style={styles.listItem} key={items.key}>{item.name}</Text>} style={{width: '100%'}}*/}
            {/*    keyExtractor={item => String(item.id)}*/}
            {/*>*/}
            {/*</FlatList>*/}

            {/*<ActivityIndicator size="large" color='#eee'/>*/}

            {/*<Image style={styles.photo} source={require('./assets/icon.png')} />*/}

            {/*<Image style={styles.photo} source={{ uri: 'https://picsum.photos/200/300' }} />*/}

            {/*<ImageBackground style={styles.photo} source={{ uri: 'https://picsum.photos/200/300' }}>*/}
            {/*    <Text>Texto</Text>*/}
            {/*</ImageBackground>*/}

            {/*<Modal animationType='fade' transparent={true} visible={modal} onRequestClose={() => console.log('close')}>*/}
            {/*    <View style={styles.center}>*/}
            {/*        <View style={{backgroundColor: '#e0e', flex: 1, width, height, justifyContent: 'center', padding: 20, margin: 20}}>*/}
            {/*            <Text>Modal</Text>*/}
            {/*            <Button title='Cerrar' onPress={() => setModal(!modal)} />*/}
            {/*        </View>*/}
            {/*    </View>*/}
            {/*</Modal>*/}

            {/*<Button title='Abrir Modal' onPress={() => setModal(true)} />*/}

            {/*<Button title='Abrir alert' onPress={createDialog} />*/}

            {/*<MapView style={styles.map}>*/}
            {/*    {locationState.coords ? <Marker coordinate={locationState.coords} title='titulo' description='description' /> : null}*/}
            {/*</MapView>*/}


            <Camera style={styles.camera} type={cameraType}>
                <Button title="Cambiar Camara" onPress={changeCameraType}></Button>
            </Camera>

        </View>
    );
}
